<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Room</title>
    <link rel="stylesheet" href="../../resources/styles/default.css"/>
</head>
<body>
<div id="container">
    <h2>Комната</h2>
    <hr/>
    <p id="room-number"><strong>${room.number}<sec:authorize access="hasAuthority('USER')">
        (<a href="/web/rooms/${room.number}/book">забронировать</a>)
        </sec:authorize></strong></p>
    <p>Вместительность: <span id="room-capacity">${room.capacity}</span></p>
    <c:if test="${empty room.students}">
        <p>Комната пуста</p>
    </c:if>
    <c:if test="${not empty room.students}">
        <p>Проживающие</p>
        <ul>
            <c:forEach items="${room.students}" var="student">
                <li><a href="/web/students/${student.id}">${student}</a></li>
            </c:forEach>
        </ul>
    </c:if>
    <c:if test="${fn:length(room.students) >= room.capacity}">
        <p>Мест нет</p>
    </c:if>
    <br/>

    <sec:authorize access="hasAuthority('ADMIN')">
        <div>
            <form:form method="POST" enctype="multipart/form-data" action="/file/photo/upload/${room.number}">
                <table>
                    <tr><td>Добавить фото:</td><td><input type="file" name="file" /></td></tr>
                    <tr><td></td><td><input type="submit" value="Загрузить" /></td></tr>
                    <tr><td style="color: red">${message}</td></tr>
                </table>
            </form:form>
        </div>
    </sec:authorize>

    <form:form action="/file/photo/${room.number}" method="get">
        <button type="submit">Фото комнаты</button>
    </form:form>
    <div style="color: red">${photoMessage}</div>
    <%@include file="_menu.jsp"%>
</div>
</body>
</html>