<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="ua.belaya.community.domain.Role" %>
<%@ page contentType="text/html;charset=utf-8" %>

<html>
<head>
    <title>Community. Student</title>
    <link rel="stylesheet" href="../../resources/styles/default.css"/>
</head>
<body>
<div id="container">
    <sec:authentication var="principal" property="principal"/>
    <h2>Студент</h2>
    <hr/>
    <p id="student-full-name"><strong>${student.lastName} ${student.firstName} ${student.patronymic}</strong>
        <c:if test="${principal.username == student.email}">
            (<a href="/web/students/${student.id}/profile">профиль</a>)
        </c:if>
    </p>

    <p>Комната: <span id="student-room"><a href="/web/rooms/${student.room.number}">${student.room}</a></span></p>

    <p id="student-email">${student.email}</p>
    <c:if test="${principal.username == student.email}">
        <c:if test="${student.room != null}">
        <form:form action="/web/deleteFromRoom/${student.id}" method="post">
            <button type="submit">Удалиться из комнаты</button>
        </form:form>
        </c:if>
        <form:form action="/web/selfDeleteStudent/${student.id}" method="post">
            <button type="submit">Удалиться из базы данных</button>
        </form:form>
    </c:if>
    <br/>
    <sec:authorize access="hasAuthority('ADMIN')">
        <p><label style="color: royalblue">Телефон:</label> ${student.telNumber}</p>

        <p><label style="color: royalblue">Домашний адрес:</label> ${student.address.country}, ${student.address.region}
            обл, г. ${student.address.city}, ул. ${student.address.street}, д. ${student.address.house},
            кв. ${student.address.flat}</p>

        <c:if test="${student.payment == true}">
            <p><label style="color: royalblue">Оплата:</label> + (оплатил/a)

            <form action="/web/students/${student.id}/paymentDel" method="post">
                <button type="submit">Не оплатил/a</button>
            </form>
        </c:if>

        <c:if test="${student.payment == false}">
            <p><label style="color: royalblue">Оплата:</label> - (не оплатил/a)

            <form action="/web/students/${student.id}/paymentAdd" method="post">
                <button type="submit">Оплатил/a</button>
            </form>
        </c:if>


        <form:form action="/web/deleteFromRoom/${student.id}" method="post">
            <button type="submit">Удалить из комнаты</button>
        </form:form>


        <form:form action="/web/deleteStudent/${student.id}" method="post">
            <button type="submit">Удалить из базы данных</button>
        </form:form>
    </sec:authorize>

    <%@include file="_menu.jsp" %>
</div>
</body>
</html>