<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=utf-8" %>

<html>
<head>
  <title>Community. Registration</title>
  <link rel="stylesheet" href="../../resources/styles/default.css">
</head>
<body>

<div id="container">
  <form:form action="/web/registration/admin" method="post" modelAttribute="admin">
    <h2>Зарегистрируйте администратора</h2>
    <hr/>
    <table>
      <tr>
        <td>
          <form:label for="username" path="email">E-mail</form:label>
        </td>
        <td>
          <form:input type="text" name="username" placeholder="e-mail" path="email"/>
          <br/><form:errors path="email" cssClass="errorMessage"/>
          <label style="color: red">${emailError}</label>
        </td>
      </tr>
      <tr>
        <td>
          <form:label for="password" path="password">Пароль</form:label>
        </td>
        <td>
          <form:input type="password" name="password" placeholder="password" path="password"/>
          <br/><form:errors path="password" cssClass="errorMessage"/>
        </td>
      </tr>
      <tr>
        <td>
          <form:label for="lastName" path="lastName">Фамилия</form:label>
        </td>
        <td>
          <form:input type="text" name="lastName" placeholder="last name" path="lastName"/>
          <br/><form:errors path="lastName" cssClass="errorMessage"/>
        </td>
      </tr>
      <tr>
        <td>
          <form:label for="firstName" path="firstName">Имя</form:label>
        </td>
        <td>
          <form:input type="text" name="firstName" placeholder="first name" path="firstName"/>
          <br/><form:errors path="firstName" cssClass="errorMessage"/>
        </td>
      </tr>
      <tr>
        <td>
          <form:label for="patronymic" path="patronymic">Отчество</form:label>
        </td>
        <td>
          <form:input type="text" name="patronymic" placeholder="patronymic" path="patronymic"/>
          <br/><form:errors path="patronymic" cssClass="errorMessage"/>
        </td>
      </tr>
      <tr>
        <td>
          <form:label for="telNumber" path="telNumber">Номер телефона</form:label>
        </td>
        <td>
          <form:input type="text" name="telNumber" placeholder="tel number" path="telNumber"/>
          <br/><form:errors path="telNumber" cssClass="errorMessage"/>
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit">Зарегистрировать</button>
        </td>
      </tr>
    </table>
  </form:form>
  <br/>
  <%@include file="_menu.jsp" %>
</div>
</body>
</html>