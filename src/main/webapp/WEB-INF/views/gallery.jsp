<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Gallery</title>
    <link rel="stylesheet" href="../../resources/styles/default.css"/>
</head>
<body>
<div id="container">
    <h2 align="center">Комната No: ${room.number}</h2>
    <table style="margin: auto">
        <tr>
            <td></td>
            <td><p align="center">
                <img src="${photo}" style=" border: double black 2px; width: 500px; height: 300px;"></p>
            </td>
            <td></td>
        </tr>
        <tr>
            <td><form:form action="/file/photo/prev/${room.number}" method="get">
                <button type="submit"><--------</button>
            </form:form></td>
            <td></td>
            <td><form:form action="/file/photo/next/${room.number}" method="get">
                <button type="submit">--------></button>
            </form:form></td>
        </tr>
        <tr>
            <td>
                <form action="/web/rooms/${room.number}">
                    <button type="submit">Назад</button>
                </form>
            </td>
            <td></td>
            <td>
                <sec:authorize access="hasAuthority('ADMIN')">
                    <form action="/file/photo/delete/${room.number}" method="post">
                        <button type="submit">Удалить</button>
                    </form>
                </sec:authorize>
            </td>
        </tr>
    </table>

    <br/>
    <%@include file="_menu.jsp" %>
</div>
</body>
</html>
