<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Student profile</title>
    <link rel="stylesheet" href="../../../resources/styles/default.css"/>
</head>
<body>
<sec:authentication var="principal" property="principal"/>
<div id="container">

    <h2>Профиль</h2>
    <hr/>
    <form:form action="/web/students/${id}/profile" method="post" commandName="studentAddress">
        <table>
            <tr>
                <td>
                    <form:label for="username" path="email">E-mail</form:label>
                </td>
                <td>
                    <form:input type="text" name="username" placeholder="e-mail" path="email"/>
                    <br/><form:errors path="email" cssClass="errorMessage"/>
                    <label style="color: red">${emailError}</label>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="password" path="password">Пароль</form:label>
                </td>
                <td>
                    <form:input type="password" name="password" placeholder="password" path="password"/>
                    <br/><form:errors path="password" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="lastName" path="lastName">Фамилия</form:label>
                </td>
                <td>
                    <form:input type="text" name="lastName" placeholder="last name" path="lastName"/>
                    <br/><form:errors path="lastName" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="firstName" path="firstName">Имя</form:label>
                </td>
                <td>
                    <form:input type="text" name="firstName" placeholder="first name" path="firstName"/>
                    <br/><form:errors path="firstName" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="patronymic" path="patronymic">Отчество</form:label>
                </td>
                <td>
                    <form:input type="text" name="patronymic" placeholder="patronymic" path="patronymic"/>
                    <br/><form:errors path="patronymic" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="telNumber" path="telNumber">Номер телефона</form:label>
                </td>
                <td>
                    <form:input type="text" name="telNumber" placeholder="tel number" path="telNumber"/>
                    <br/><form:errors path="telNumber" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="sex" path="sex">Пол</form:label>
                </td>
                <td>
                    <form:select name="sex" path="sex">
                        <option>male</option>
                        <option>female</option>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <label>Адрес:</label>
                </td>
            </tr>
            <tr>
                <br>
                <td>
                    <form:label for="country" path="country">Страна</form:label>
                </td>
                <td>
                    <form:input type="text" name="country" placeholder="country" path="country"/>
                    <br/><form:errors path="country" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="region" path="region">Область</form:label>
                </td>
                <td>
                    <form:input type="text" name="region" placeholder="region" path="region"/>
                    <br/><form:errors path="region" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="city" path="city">Город</form:label>
                </td>
                <td>
                    <form:input type="text" name="city" placeholder="city" path="city"/>
                    <br/><form:errors path="city" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="street" path="street">Улица</form:label>
                </td>
                <td>
                    <form:input type="text" name="street" placeholder="street" path="street"/>
                    <br/><form:errors path="street" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="house" path="house">Дом</form:label>
                </td>
                <td>
                    <form:input type="text" name="house" placeholder="house" path="house"/>
                    <br/><form:errors path="house" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label for="flat" path="flat">Квартира</form:label>
                </td>
                <td>
                    <form:input type="text" name="flat" placeholder="flat" path="flat"/>
                    <br/><form:errors path="flat" cssClass="errorMessage"/>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Сохранить</button>
                </td>
                <td>
                    <button type="reset">Отмена</button>
                </td>
            </tr>
            </table>
        </form:form>
    <br/>
    <%@include file="_menu.jsp" %>
</div>
</body>
</html>