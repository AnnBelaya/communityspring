<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Admins</title>
    <link rel="stylesheet" href="../../resources/styles/default.css"/>
</head>
<body>
<sec:authentication var="principal" property="principal"/>
<div id="container">
    <h2>Администраторы</h2>
    <hr/>
    <c:if test="${empty admins}">
        <p>База данных пуста</p>
    </c:if>
    <c:if test="${not empty admins}">
        <table>
            <tr>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>E-mail</th>
                <th>Телефон</th>
            </tr>
            <c:forEach items="${admins}" var="admin">
                <tr>
                    <td>${admin.lastName}</td>
                    <td>${admin.firstName}</td>
                    <td>${admin.patronymic}</td>
                    <td>${admin.email}</td>
                    <td>${admin.telNumber}</td>

                    <td>
                        <c:if test="${principal.username == admin.email}">
                        (<a href="/web/admins/update/${admin.id}">профиль</a>)
                        </c:if>
                    <td>
                </tr>
            </c:forEach>
        </table>
        <sec:authorize access="hasAuthority('ADMIN')">
        <form action="/web/registration/admin">
            <button type="submit">Добавить администратора</button>
        </form>
        </sec:authorize>
    </c:if>
    <br/>
    <%@include file="_menu.jsp" %>
</div>
</body>
</html>