<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Students</title>
    <link rel="stylesheet" href="../../resources/styles/default.css"/>
</head>
<body>
<sec:authentication var="principal" property="principal"/>
<div id="container">
    <h2>Студенты</h2>
    <hr/>
    <c:if test="${empty students}">
        <p>База данных пуста</p>
    </c:if>
    <c:if test="${not empty students}">
        <table>
            <tr>
                <form action="/web/students/search" method="get">
                    <td>
                        <input type="text" placeholder="студент" name="search">
                            ${notFound}
                    </td>
                    <td>
                        <input type="submit" name="sumbitB" value="Поиск">
                    </td>
                </form>
            </tr>
            <tr>
                <th>Полное имя</th>
                <th>Комната</th>
                <sec:authorize access="hasAuthority('ADMIN')">
                    <th>Оплата</th>
                </sec:authorize>
            </tr>
            <form:form action="/web/payment" method="post">
            <c:forEach items="${students}" var="student">
            <c:if test='${principal.username == student.email}'>
                <c:set value="authUser" var="authUser"/>
            </c:if>
            <tr>
                <td><a href="/web/students/${student.id}" class="${authUser}">
                        ${student.lastName} ${student.firstName} ${student.patronymic}
                </a></td>
                <c:if test="${student.room != null}">
                    <td><a href="/web/rooms/${student.room.number}">
                            ${student.room.number}
                    </a></td>
                </c:if>
                <c:if test="${student.room == null}">
                    <td>нет
                    </td>
                </c:if>
                <sec:authorize access="hasAuthority('ADMIN')">
                    <td>
                        <c:if test='${student.payment == true}'>
                            +
                        </c:if>
                        <c:if test='${student.payment == false}'>
                            -
                        </c:if>
                    </td>
                </sec:authorize>
                <c:set value="" var="authUser"/>
                </c:forEach>
                </form:form>
            </tr>
        </table>
    </c:if>
    <br/>
    <%@include file="_menu.jsp" %>
</div>
</body>
</html>