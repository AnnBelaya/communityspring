<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <title>Community. Login</title>
    <link rel="stylesheet" href="../../resources/styles/default.css">
</head>
<body>

<div id="container">
    <form action="/auth/login" method="post">
        <h2>Авторизируйтесь</h2>
        <hr/>
        <table>
            <tr>
                <td>
                    <label for="username">E-mail</label>
                </td>
                <td>
                    <input type="text" id="username" name="username" placeholder="e-mail" required autofocus/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">Пароль</label>
                </td>
                <td>
                    <input type="password" id="password" name="password" placeholder="password" required/>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Войти</button>
                </td>
                <td>
                    <a href="/auth/registration">Регистрация</a>
                </td>
            </tr>
        </table>
        <p>
        <c:if test="${param.error != null}">
            <span style="color: #8b0000">
                Неверный логин или пароль.
            </span>
        </c:if>
        <c:if test="${param.logout != null}">
            <span>
                Вы вышли из акаунта.
            </span>
        </c:if>
        </p>
    </form>
</div>
</body>
</html>