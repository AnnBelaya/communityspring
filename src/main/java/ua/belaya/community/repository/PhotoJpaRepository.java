package ua.belaya.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.belaya.community.domain.Photo;

/**
 * @author Anna Belaya
 */
public interface PhotoJpaRepository extends JpaRepository<Photo,Integer>{
}
