package ua.belaya.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.belaya.community.domain.Address;

/**
 * @author Anna Belaya
 */
public interface AddressJpaRepository extends JpaRepository<Address,Integer>{
}
