package ua.belaya.community.domain;

/**
 * @author Anna Belaya
 */
public enum Sex {
    MALE, FEMALE
}
