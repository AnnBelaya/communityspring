package ua.belaya.community.domain;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Anna Belaya
 */
public class StudentAddressDTO {
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String lastName;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String firstName;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String patronymic;

    private String sex;

    @Size(min = 10, max = 10)
    @Pattern(regexp = "[0-9-]+")
    private String telNumber;

    private boolean payment;

    @Email
    private String email;

    @Size(min = 4, max = 60)
    private String password = "0000";

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String country;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String region;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String city;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    private String street;

    @Size(min = 1)
    @Pattern(regexp = "[А-Яа-я0-9-]+")
    private String house;

    @Size(min = 1)
    @Pattern(regexp = "[0-9-]+")
    private String flat;


    public StudentAddressDTO(){}

    public StudentAddressDTO(Student student, Address address){
        this.id = student.getId();
        this.lastName = student.getLastName();
        this.firstName = student.getFirstName();
        this.patronymic = student.getPatronymic();
        this.telNumber = student.getTelNumber();
        this.email = student.getEmail();
        this.password = student.getPassword();
        this.country= address.getCountry();
        this.region = address.getRegion();
        this.city = address.getCity();
        this.street = address.getStreet();
        this.house = address.getHouse();
        this.flat = address.getFlat();

        if (Sex.MALE.equals(student.getSex())){
            sex = "male";
        }
        else if(Sex.FEMALE.equals(student.getSex())){
            sex = "female";
        }
    }

    public Student getStudent(){
        Sex studentSex = null;

        if ("male".equals(sex)){
            studentSex = Sex.MALE;
        }
        else if ("female".equals(sex)){
            studentSex = Sex.FEMALE;
        }
        Student student = new Student(lastName,firstName,patronymic,studentSex,telNumber,null);
        student.setEmail(email);
        student.setPassword(password);

        return student;
    }

    public Address getAddress(){
        return new Address(country,region,city,street,house,flat);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public boolean isPayment() {
        return payment;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }
}
