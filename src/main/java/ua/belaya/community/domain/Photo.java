package ua.belaya.community.domain;

import javax.persistence.*;
import java.io.File;

/**
 * @author Anna Belaya
 */

@Entity
@Table(name = "photoes")
public class Photo {
    @Id
    @GeneratedValue
    @Column(name = "photo_id")
    private int id;
    @Column(name = "photo_file")
    private String file;

    @ManyToOne
    @JoinColumn(name = "photo_room")
    private Room room;

    public Photo(){}

    public Photo(String file) {
        this.file = file;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
