package ua.belaya.community.domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna Belaya
 */

@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue
    @Column(name = "address_id")
    private Long id;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    @Column(name = "address_country")
    private String country;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    @Column(name = "address_region")
    private String region;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    @Column(name = "address_city")
    private String city;

    @Size(min = 2, max = 20)
    @Pattern(regexp = "[А-Я][а-я]+")
    @Column(name = "address_street")
    private String street;

    @Size(min = 1)
    @Pattern(regexp = "[А-Яа-я0-9-]+")
    @Column(name = "address_house")
    private String house;

    @Size(min = 1)
    @Pattern(regexp = "[0-9-]+")
    @Column(name = "address_flat")
    private String flat;

    @OneToMany(mappedBy = "address")
    private List<Student> students = new ArrayList<>();

    public Address(){}

    public Address(String country, String region, String city, String street, String house, String flat) {
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        if (city != null ? !city.equals(address.city) : address.city != null) return false;
        if (country != null ? !country.equals(address.country) : address.country != null) return false;
        if (flat != null ? !flat.equals(address.flat) : address.flat != null) return false;
        if (house != null ? !house.equals(address.house) : address.house != null) return false;
        if (region != null ? !region.equals(address.region) : address.region != null) return false;
        if (street != null ? !street.equals(address.street) : address.street != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (house != null ? house.hashCode() : 0);
        result = 31 * result + (flat != null ? flat.hashCode() : 0);
        return result;
    }
}
