package ua.belaya.community;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;

/**
 * @author Anna Belaya
 */

@SpringBootApplication
public class CommunityApp {

    public static void main(String[] args) {
        SpringApplication.run(CommunityApp.class, args);
    }

}
