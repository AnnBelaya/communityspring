package ua.belaya.community.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ua.belaya.community.domain.*;
import ua.belaya.community.repository.AddressJpaRepository;
import ua.belaya.community.repository.RoomJpaRepository;
import ua.belaya.community.repository.StudentJpaRepository;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Anna Belaya
 */

@Component
public class BootStrap {

    @Autowired
    private StudentJpaRepository studentRepository;

    @Autowired
    private RoomJpaRepository roomRepository;

    @Autowired
    private AddressJpaRepository addressRepository;

    @Autowired
    private Environment environment;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() throws IOException{
        System.setProperty("console.encoding","Cp866");

        Room r1 = new Room("100", 4);
        Room r2 = new Room("101", 3);
        Room r3 = new Room("103", 3);

        r1.setSex(Sex.MALE);
        r2.setSex(Sex.MALE);

        roomRepository.save(r1);
        roomRepository.save(r2);
        roomRepository.save(r3);

        Address address = new Address("Украина", "Днепропетровская", "Днепр", "Вакуленчука", "8а", "11");

        addressRepository.save(address);

        String telNumber = "0504782942";
        Student s1 = new Student("Иванов", "Иван", "Иванович", Sex.MALE, telNumber, r1);
        Student s2 = new Student("Кириллов", "Кирилл", "Кириллович", Sex.MALE, telNumber, r2);
        Student s3 = new Student("Алексеев", "Алексей", "Алексеевич", Sex.MALE, telNumber, null);
        Student s4 = new Student("Михайлов", "Михаил", "Михаилович", Sex.MALE, telNumber, r2);

        s1.setEmail("ivanov@box.com");
        s1.setPassword(passwordEncoder.encode("00000000"));

        Student admin = new Student("Админ",
                "Админ", "Админ", Sex.FEMALE,
                "0000000000", null);

        admin.setEmail(environment.getProperty("admin.login"));
        admin.setPassword(passwordEncoder.encode(environment.getProperty("admin.password")));
        admin.setRole(Role.ADMIN);

        s1.setAddress(address);
        s2.setAddress(address);
        s3.setAddress(address);
        s4.setAddress(address);

        studentRepository.save(s1);
        studentRepository.save(s2);
        studentRepository.save(s3);
        studentRepository.save(s4);
        studentRepository.save(admin);
    }
}
