package ua.belaya.community.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.belaya.community.domain.Address;
import ua.belaya.community.domain.Student;
import ua.belaya.community.repository.AddressJpaRepository;
import ua.belaya.community.repository.StudentJpaRepository;

/**
 * @author Anya Belaya
 */
@Component
public class InfoCheckerImpl implements InfoChecker{
    @Autowired
    private AddressJpaRepository addressRepository;
    @Autowired
    private StudentJpaRepository studentRepository;

    public Address checkAddress(Address address, Student student) {
        for (Address actualAddress : addressRepository.findAll()) {
            if (actualAddress.equals(address)) {
                return actualAddress;
            }
        }

        address.setId(student.getAddress().getId());
        return addressRepository.save(address);
    }

    public boolean isUniqueStudent(Student student) {
        for (Student actualStudent : studentRepository.findAll()) {
            if (!student.getId().equals(actualStudent.getId()) && student.getEmail().equals(actualStudent.getEmail())) {
                return false;
            }
        }
        return true;
    }
}
