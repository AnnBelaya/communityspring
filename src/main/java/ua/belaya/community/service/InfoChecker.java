package ua.belaya.community.service;

import ua.belaya.community.domain.Address;
import ua.belaya.community.domain.Student;

/**
 * @author Anya Belaya
 */
public interface InfoChecker {
    Address checkAddress(Address address, Student student);
    boolean isUniqueStudent(Student student);
}
