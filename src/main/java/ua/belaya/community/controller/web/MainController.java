package ua.belaya.community.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.belaya.community.config.security.SecurityUser;
import ua.belaya.community.domain.*;
import ua.belaya.community.repository.AddressJpaRepository;
import ua.belaya.community.repository.RoomJpaRepository;
import ua.belaya.community.repository.StudentJpaRepository;
import ua.belaya.community.service.InfoChecker;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna Belaya
 */

@Controller
@RequestMapping("/web")
public class MainController {

    @Autowired
    private StudentJpaRepository studentRepository;

    @Autowired
    private RoomJpaRepository roomRepository;

    @Autowired
    private InfoChecker infoChecker;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @ModelAttribute("authenticatedUser")
    public Student authenticatedUser() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String email = authentication.getName();
        return studentRepository.findStudentByEmail(email);
    }

    @ModelAttribute
    public Room emptyRoom() {
        return new Room();
    }

    @RequestMapping("/students")
    public ModelAndView students() {
        Sort sort = new Sort("room.number", "lastName", "firstName", "patronymic");
        List<Student> students = studentRepository.findStudentByRole(Role.USER, sort);
        return new ModelAndView("students", "students", students);
    }

    @RequestMapping("/students/{id}")
    public ModelAndView student(@PathVariable("id") Long id) {
        Student student = studentRepository.findOne(id);
        return new ModelAndView("student", "student", student);
    }

    @RequestMapping("/students/{id}/profile")
    public ModelAndView studentProfile(@PathVariable("id") Long id,
                                       @ModelAttribute("authenticatedUser") Student authenticated) {
        //prevent accessing profile of other user
        Student actual = studentRepository.findOne(id);
        if (!actual.getEmail().equals(authenticated.getEmail())) {
            return new ModelAndView("redirect:/web/students/" + id);
        }

        return new ModelAndView("studentprofile", "studentAddress", new StudentAddressDTO(authenticated, authenticated.getAddress()));
    }

    @RequestMapping(value = "/students/{id}/profile", method = RequestMethod.POST)
    public ModelAndView studentProfileProcess(@PathVariable("id") Long id,
                                              @Valid @ModelAttribute("studentAddress") StudentAddressDTO studentAddress,
                                              BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("studentprofile");
        }
        Student student = studentAddress.getStudent();
        student.setId(id);

        if (!infoChecker.isUniqueStudent(student)) {
            return new ModelAndView("studentprofile", "emailError", "E-mail is already in use");
        }

        Student actual = studentRepository.findOne(id);
        Address address = infoChecker.checkAddress(studentAddress.getAddress(), actual);

        student.setRoom(actual.getRoom());
        student.setRole(actual.getRole());
        student.setAddress(address);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser userDetails = (SecurityUser) authentication.getPrincipal();
        userDetails.setEmail(student.getEmail());

        if(!actual.getPassword().equals(student.getPassword())){
            student.setPassword(passwordEncoder.encode(student.getPassword()));
            userDetails.setPassword(passwordEncoder.encode(student.getPassword()));
        }
        studentRepository.save(student);

        return new ModelAndView("redirect:/web/students/" + id);
    }

    @RequestMapping("/rooms")
    public ModelAndView rooms() {
        return new ModelAndView("rooms", "rooms", roomRepository.findAll(new Sort("number")));
    }

    @RequestMapping("/rooms/{number}")
    public ModelAndView room(@PathVariable("number") String number,
                             @ModelAttribute("message") String message) {
        ModelAndView modelAndView = new ModelAndView("room", "room", roomRepository.findOne(number));
        if (message != null) {
            modelAndView.addObject("message", message);
        }

        return modelAndView;
    }

    @RequestMapping("/rooms/{number}/book")
    public String bookRoom(@PathVariable("number") String number,
                           @ModelAttribute("authenticatedUser") Student student) {
        Room room = roomRepository.findOne(number);

        if (room.getStudents().size() == 0) {
            room.setSex(student.getSex());
            roomRepository.save(room);

            student.setRoom(room);
            studentRepository.save(student);
        } else if (room.getCapacity() > room.getStudents().size() && student.getSex().equals(room.getSex())) {
            student.setRoom(room);
            studentRepository.save(student);
        }

        return "redirect:/web/rooms/" + number;
    }

    @RequestMapping("/rooms/{number}/remove")
    public String removeRoom(@PathVariable("number") String number,
                             @ModelAttribute("authenticatedUser") Student student) {
        if (student.getRole().equals(Role.ADMIN)) {
            Room room = roomRepository.getOne(number);
            for (Student s : room.getStudents()) {
                s.setRoom(null);
                studentRepository.save(s);
            }
            roomRepository.delete(room);
        }

        return "redirect:/web/rooms";
    }

    @RequestMapping("/rooms/{number}/clear")
    public String clearRoom(@PathVariable("number") String number,
                            @ModelAttribute("authenticatedUser") Student student) {
        if (student.getRole().equals(Role.ADMIN)) {
            Room room = roomRepository.getOne(number);
            for (Student s : room.getStudents()) {
                s.setRoom(null);
                studentRepository.save(s);
            }
        }

        return "redirect:/web/rooms";
    }

    @RequestMapping("/rooms/add")
    public ModelAndView addRoom() {
        return new ModelAndView("addroom");
    }

    @RequestMapping(value = "/rooms/add", method = RequestMethod.POST)
    public ModelAndView addRoomProcess(@Valid @ModelAttribute("room") Room room,
                                       BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("addroom");
        }

        roomRepository.save(room);
        return new ModelAndView("redirect:/web/rooms");
    }

    @RequestMapping("/admins")
    public ModelAndView admins() {
        Sort sort = new Sort("lastName", "firstName", "patronymic");
        List<Student> admins = studentRepository.findStudentByRole(Role.ADMIN, sort);
        return new ModelAndView("admins", "admins", admins);
    }

    @RequestMapping(value = "/students/{id}/paymentDel", method = RequestMethod.POST)
    public ModelAndView paymentDelete(@PathVariable("id") Long id) {
        Student student = studentRepository.findOne(id);
        student.setPayment(false);
        studentRepository.save(student);

        return new ModelAndView("redirect:/web/students/" + id);
    }

    @RequestMapping(value = "/students/{id}/paymentAdd", method = RequestMethod.POST)
    public ModelAndView paymentAdd(@PathVariable("id") Long id) {
        Student student = studentRepository.findOne(id);
        student.setPayment(true);
        studentRepository.save(student);

        return new ModelAndView("redirect:/web/students/" + id);
    }

    @RequestMapping(value = "/deleteFromRoom/{id}", method = RequestMethod.POST)
    public ModelAndView deleteFromRoom(@PathVariable("id") Long id) {
        Student student = studentRepository.findOne(id);

        if (student.getRoom() != null) {
            Room room = student.getRoom();

            student.setRoom(null);
            studentRepository.save(student);

            if (room.getStudents().isEmpty()) {
                room.setSex(null);
                roomRepository.save(room);
            }
        }
        return new ModelAndView("student", "student", studentRepository.findOne(id));
    }

    @RequestMapping(value = "/deleteStudent/{id}", method = RequestMethod.POST)
    public ModelAndView deleteStudent(@PathVariable("id") Long id) {
        studentRepository.delete(id);

        return new ModelAndView("students", "students", studentRepository.findStudentByRole(Role.USER,
                new Sort("room.number", "lastName", "firstName", "patronymic")));
    }

    @RequestMapping(value = "/selfDeleteStudent/{id}", method = RequestMethod.POST)
    public ModelAndView selfDeleteStudent(@PathVariable("id") Long id) {
        studentRepository.delete(id);

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/students/search")
    public ModelAndView searchStudent(HttpServletRequest request) {
        String fullName = request.getParameter("search");
        String[] parts = fullName.split(" ");

        if (parts.length > 3 || parts.length == 0) {
            return new ModelAndView("redirect:/web/students");
        }

        List<Student> students = new ArrayList<>();

        if (parts.length == 1) {
            for (Student student : studentRepository.findAll()) {
                if (!student.getRole().equals(Role.USER)) {
                    continue;
                }

                if (student.getFirstName().equals(parts[0]) ||
                        student.getLastName().equals(parts[0]) ||
                        student.getPatronymic().equals(parts[0])) {
                    students.add(student);
                }
            }
        } else if (parts.length == 2) {
            for (Student student : studentRepository.findAll()) {
                if (!student.getRole().equals(Role.USER)) {
                    continue;
                }

                String firstName = student.getFirstName();
                String lastName = student.getLastName();
                String patronymic = student.getPatronymic();

                if ((firstName.equals(parts[0]) || lastName.equals(parts[0]) || patronymic.equals(parts[0])) &&
                        (firstName.equals(parts[1]) || lastName.equals(parts[1]) || patronymic.equals(parts[1]))) {
                    students.add(student);
                }
            }
        } else {
            for (Student student : studentRepository.findAll()) {
                if (!student.getRole().equals(Role.USER)) {
                    continue;
                }

                String firstName = student.getFirstName();
                String lastName = student.getLastName();
                String patronymic = student.getPatronymic();

                if ((firstName.equals(parts[0]) || lastName.equals(parts[0]) || patronymic.equals(parts[0])) &&
                        (firstName.equals(parts[1]) || lastName.equals(parts[1]) || patronymic.equals(parts[1])) &&
                        firstName.equals(parts[2]) || lastName.equals(parts[2]) || patronymic.equals(parts[2])) {

                    students.add(student);
                }
            }
        }

        return new ModelAndView("students", "students", students);
    }

    @RequestMapping("/registration/admin")
    public ModelAndView registerAdmin() {
        return new ModelAndView("registrationadmin", "admin", new Student());
    }

    @RequestMapping(value = "/registration/admin", method = RequestMethod.POST)
    public ModelAndView registerAdminProcess(@Valid @ModelAttribute("admin") Student admin,
                                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("registrationadmin");
        }

        for (Student actualStudent : studentRepository.findAll()) {
            if (admin.getEmail().equals(actualStudent.getEmail())) {
                return new ModelAndView("registrationadmin", "emailError", "Email is already in use");
            }
        }

        admin.setSex(Sex.MALE);
        admin.setRole(Role.ADMIN);
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));

        studentRepository.save(admin);

        return new ModelAndView("redirect:/web/admins");
    }

    @RequestMapping("/admins/update/{id}")
    public ModelAndView adminsUpdate(@PathVariable("id") Long id,
                                     @ModelAttribute("authenticatedUser") Student authenticated) {
        //prevent accessing profile of other user
        Student actual = studentRepository.findOne(id);
        if (!actual.getEmail().equals(authenticated.getEmail())) {
            return new ModelAndView("redirect:/web/students/" + id);
        }

        return new ModelAndView("adminsupdate", "admin", authenticated);
    }

    @RequestMapping(value = "/admins/{id}/profile", method = RequestMethod.POST)
    public ModelAndView adminProfileProcess(@PathVariable("id") Long id,
                                            @Valid @ModelAttribute("admin") Student admin,
                                            BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("adminsupdate");
        }

        admin.setId(id);

        if (!infoChecker.isUniqueStudent(admin)) {
            return new ModelAndView("adminsUpdate", "emailError", "E-mail is already in use");
        }

        Student actual = studentRepository.findOne(id);

        admin.setRole(actual.getRole());
        admin.setSex(actual.getSex());
        admin.setAddress(actual.getAddress());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser userDetails = (SecurityUser) authentication.getPrincipal();
        userDetails.setEmail(admin.getEmail());

        if (!actual.getPassword().equals(admin.getPassword())){
            admin.setPassword(passwordEncoder.encode(admin.getPassword()));
            userDetails.setPassword(passwordEncoder.encode(admin.getPassword()));
        }

        studentRepository.save(admin);

        return new ModelAndView("redirect:/web/admins");
    }
}
