package ua.belaya.community.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ua.belaya.community.domain.Photo;
import ua.belaya.community.domain.Room;
import ua.belaya.community.repository.PhotoJpaRepository;
import ua.belaya.community.repository.RoomJpaRepository;

import java.io.File;
import java.io.IOException;

/**
 * @author Anna Belaya
 */

@Controller
@RequestMapping("/file/photo")
public class FileUploadController {
    @Autowired
    private RoomJpaRepository roomRepository;
    @Autowired
    private PhotoJpaRepository photoRepository;

    private int indexPhoto;

    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST)
    public ModelAndView uploadFile(@RequestParam("file") MultipartFile multipartFile,
                                   @PathVariable("id") String id) {
        Room room = roomRepository.getOne(id);

        if (multipartFile.isEmpty()) {
            return new ModelAndView("redirect:/web/rooms/" + id);
        }

        String fileName = Math.random() * 10 + multipartFile.getOriginalFilename();

        String extension = fileName.substring(fileName.lastIndexOf("."), fileName.length());

        if (!".jpg".equals(extension) && !".png".equals(extension)) {
            return new ModelAndView("redirect:/web/rooms/" + id, "message", "неверный форман! нужен .jpg или .png");
        }

        File file = new File(System.getProperty("user.dir") + "\\src\\main\\webapp\\resources\\files\\" + fileName);
        Photo photo = new Photo(fileName);

        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            return new ModelAndView("redirect:/web/rooms/" + id, "message", "невозможно загрузить файл");
        }
        photo.setRoom(room);
        photoRepository.save(photo);

        return new ModelAndView("redirect:/web/rooms/" + id, "message", "фото сохранено");
    }

    @RequestMapping(value = "/{id}")
    public ModelAndView getPhotos(@PathVariable("id") String id) {
        Room room = roomRepository.findOne(id);

        if (isPhotoListEmpty(room)) {
            ModelAndView modelAndView = new ModelAndView("room", "room", room);

            modelAndView.addObject("photoMessage", "нет фото");

            return modelAndView;
        }

        if (indexPhoto >= room.getPhotos().size()) {
            indexPhoto = 0;
        }

        ModelAndView modelAndView = new ModelAndView("gallery", "room", room);

        modelAndView.addObject("photo", "../../resources/files/" + room.getPhotos().get(indexPhoto).getFile());
        return modelAndView;
    }

    @RequestMapping(value = "/next/{id}")
    public ModelAndView getNextPhoto(@PathVariable("id") String id) {
        Room room = roomRepository.findOne(id);
        if (isPhotoListEmpty(room)) {
            return new ModelAndView("redirect:/web/rooms/" + id);
        }

        if (indexPhoto == roomRepository.findOne(id).getPhotos().size() - 1) {
            indexPhoto = 0;
        } else {
            indexPhoto++;
        }

        return new ModelAndView("redirect:/file/photo/" + id);
    }

    @RequestMapping(value = "/prev/{id}")
    public ModelAndView getPrevPhoto(@PathVariable("id") String id) {
        if (indexPhoto == 0) {
            indexPhoto = roomRepository.findOne(id).getPhotos().size() - 1;
        } else {
            indexPhoto--;
        }

        return new ModelAndView("redirect:/file/photo/" + id);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public ModelAndView deletePhoto(@PathVariable("id") String id) {
        Photo photo = roomRepository.findOne(id).getPhotos().get(indexPhoto);

        new File(System.getProperty("user.dir") + "\\src\\main\\webapp\\resources\\files\\" + photo.getFile()).delete();
        //photo.getFile().delete();
        photoRepository.delete(photo);

        return new ModelAndView("redirect:/file/photo/next/" + id);
    }

    boolean isPhotoListEmpty(Room room) {
        return room.getPhotos().isEmpty();
    }
}
