package ua.belaya.community.controller.web;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.belaya.community.domain.Address;
import ua.belaya.community.domain.Sex;
import ua.belaya.community.domain.Student;
import ua.belaya.community.domain.StudentAddressDTO;
import ua.belaya.community.repository.AddressJpaRepository;
import ua.belaya.community.repository.StudentJpaRepository;
import ua.belaya.community.service.InfoChecker;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Anna Belaya
 */

@Controller
@RequestMapping("/auth")
public class AuthorizationController {

    @Autowired
    private StudentJpaRepository studentRepository;

    @Autowired
    private InfoChecker infoChecker;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @ModelAttribute()
    private Student emptyStudent() {
        return new Student();
    }

    @RequestMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("login");
    }

    @RequestMapping("/registration")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("studentAddress", new StudentAddressDTO());
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registerProcess(@Valid @ModelAttribute("studentAddress") StudentAddressDTO studentAddress,
                                        BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("registration");
        }

        Student student = studentAddress.getStudent();

        if (!infoChecker.isUniqueStudent(student)) {
            return new ModelAndView("registration", "emailError", "Email is already in use");
        }

        student.setPassword(passwordEncoder.encode(student.getPassword()));

        Address address = infoChecker.checkAddress(studentAddress.getAddress(),student);

        student.setAddress(address);
        studentRepository.save(student);

        return new ModelAndView("login");
    }
}
